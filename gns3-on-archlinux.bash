#!/bin/bash

SOURCES='/tmp/gns3'

sudo pacman -Syy python-pip wget git python-pyqt5 qt5-tools qt5-multimedia qt5-websockets python-pyzmq qt5-svg bison flex gcc cmake libelf libpcap
sudo pip install --upgrade pip
sudo pip install -U tornado ws4py setuptools netifaces zmq dev
sudo pip install -U ws4py
sudo pip install pyqt5

if [ -d "$SOURCES" ]; then
    sudo rm -rf $SOURCES
fi

mkdir $SOURCES && cd $SOURCES

# GNS3 GUI
echo
echo
echo "Installing GNS3 GUI"
echo
sudo git clone https://github.com/GNS3/gns3-gui.git
cd gns3-gui
sudo python setup.py install
cd ..

# GNS3 Server
echo
echo
echo "Installing GNS3 Server"
echo
sudo git clone https://github.com/GNS3/gns3-server.git
cd gns3-server
sudo python setup.py install
cd ..

# Dynamips
echo
echo
echo "Installing Dynamips"
echo
sudo git clone git://github.com/GNS3/dynamips.git
cd dynamips
sudo mkdir build
cd build
sudo cmake ..
sudo make
sudo make install
cd ../..

# Iniparser
echo
echo
echo "Installing Iniparser"
echo
sudo git clone http://github.com/ndevilla/iniparser.git
cd iniparser
sudo make
sudo cp libiniparser.* /usr/lib/
sudo cp src/iniparser.h /usr/local/include
sudo cp src/dictionary.h /usr/local/include
cd ..

# Iouyap
echo
echo
echo "Installing Iouyap"
echo
sudo git clone https://github.com/GNS3/iouyap.git
cd iouyap
sudo make
sudo make install
cd ..

# VPCS
echo
echo
echo "Installing VPS"
echo
sudo git clone https://github.com/GNS3/vpcs.git
cd vpcs/src
sudo sh mk.sh
sudo cp vpcs /usr/local/bin/
sudo chmod +x /usr/local/bin/vpcs
cd ../..

# Ubridge
echo
echo
echo "Installing Ubridge"
echo
sudo git clone https://github.com/GNS3/ubridge.git
cd ubridge
sudo make
sudo make install
cd ..

# Icon and Desktop entry
echo
echo
echo "Creating a GNOME entry for GNS3"
echo
cd /usr/share/icons
sudo wget https://www.gns3.com/assets/images/logo-colour.png
sudo mv logo-colour.png gns3.png
sudo cp /tmp/gns3/gns3-gui/gns3-gui.desktop /usr/share/applications/gns3.desktop
sudo sed -i -e 's/Icon=gns3/Icon=\/usr\/share\/icons\/gns3\.png/' /usr/share/applications/gns3.desktop

sudo rm -rf mkdir $SOURCES

#REFERENCEs:

#* [How to Install Latest GNS3 on Arch Linux and Manjaro](http://computingforgeeks.com/how-to-install-latest-gns3-on-arch-linux-and-manjaro/)
