# GNS3 on Archlinux

It is a simple script that automates GNS3 installation and upgrades on Archlinux.

#### Cloning and running

```
git clone https://arbiza@bitbucket.org/arbiza/gns3-on-archlinux.git
cd gns3-on-archlinux
bash gns3-on-archlinux.bash
```

The procedures the script implements were based on instructions from:

* [How to Install Latest GNS3 on Arch Linux and Manjaro, by Kiplangat Mutai at Computing for Geeks](http://computingforgeeks.com/how-to-install-latest-gns3-on-arch-linux-and-manjaro/)
